#
# KKO 2016/2017 Conversion from image format GIF to BMP
# Author: Matej Vido, xvidom00@stud.fit.vutbr.cz
# Date:   05/2017

NAME=gif2bmp
LOGIN=xvidom00
ZIPFILEDIR=kko.proj3.$(LOGIN)
ZIPFILE=$(ZIPFILEDIR).zip
CXX=g++
CXXFLAGS=-std=c++11
DFLAGS=
#DFLAGS=-Wall -Wextra -pedantic -g -DGIF2BMP_DEBUG

all: $(NAME)

gif.o: gif.cpp gif.h
	$(CXX) $(CXXFLAGS) $(DFLAGS) $< -c -o $@

bmp.o: bmp.cpp bmp.h
	$(CXX) $(CXXFLAGS) $(DFLAGS) $< -c -o $@

$(NAME).o: gif2bmp.cpp gif2bmp.h
	$(CXX) $(CXXFLAGS) $(DFLAGS) $< -c -o $@

$(NAME): main.cpp $(NAME).o gif.o bmp.o
	$(CXX) $(CXXFLAGS) $(DFLAGS) $< $(NAME).o gif.o bmp.o -o $@

.PHONY: clean purge pack doc

clean:
	rm -f *.o

purge:
	rm -f *.o $(NAME)

pack:
	rm -f $(ZIPFILE)
	rm -rf $(ZIPFILEDIR)
	mkdir -p $(ZIPFILEDIR)
	cp Makefile main.cpp debug.h gif2bmp.cpp gif2bmp.h gif2bmp.pdf gif.cpp gif.h bmp.cpp bmp.h $(ZIPFILEDIR)/ ; true
	zip -r $(ZIPFILE) $(ZIPFILEDIR)

doc:
	make -C doc
	cp doc/doc.pdf gif2bmp.pdf
