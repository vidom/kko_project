/*
 * @brief  KKO 2016/2017 Conversion of image format GIF to BMP
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   05/2017
 * @file   bmp.cpp
 */

#define __STDC_FORMAT_MACROS

/* System and standard library headers */
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <iostream>

/* Own headers */
#include "bmp.h"

using namespace std;

Bmp::Bmp(FILE *f, uint16_t w, uint16_t h) :
    file{f},
    width{w},
    height{h},
    colors{((uint64_t)(w)) * ((uint64_t)(h)), Bmp::color{0, 0, 0, 255}},
    file_size{0}
{
    file_size = sizeof(struct bmp_header) + sizeof(struct bitmapv4_header) +
        width * height * sizeof(struct color);
}

void
Bmp::set_pixel(uint16_t row, uint16_t col, uint8_t red,
            uint8_t green, uint8_t blue, uint8_t transparent)
{
    colors[((uint64_t)(row)) * ((uint64_t)(width)) + ((uint64_t)(col))] =
        color{blue, green, red, transparent};
}

int
Bmp::write(void)
{
    size_t elems;
    struct bmp_header header;
    struct bitmapv4_header dib_header;

    header.id = BMP_ID;
    header.file_size = this->file_size;
    header.unused1 = 0;
    header.unused2 = 0;
    header.pixel_array_offset = sizeof(struct bmp_header) +
        sizeof(struct bitmapv4_header);

    dib_header.dib_header_size = sizeof(struct bitmapv4_header);
    dib_header.bitmap_width = this->width;
    dib_header.bitmap_height = this->height;
    dib_header.planes = 1;
    dib_header.bits_per_pixel = 32;
    dib_header.commpression_method = 3;
    dib_header.image_size = sizeof(color) * ((uint64_t)(this->width)) *
        ((uint64_t)(this->height));
    dib_header.horizontal_res = 2835;
    dib_header.vertical_res = 2835;
    dib_header.colors = 0;
    dib_header.important_colors = 0;
    dib_header.red_bitmask   = 0x00FF0000;
    dib_header.green_bitmask = 0x0000FF00;
    dib_header.blue_bitmask  = 0x000000FF;
    dib_header.alpha_bitmask = 0xFF000000;
    dib_header.color_space = BMP_LCS_WINDOWS_COLOR_SPACE;
    memset(dib_header.unused, 0, 48);

    elems = fwrite(&header, sizeof(header), 1, this->file);
    if (elems != 1) {
        cerr << "BMP: Could not write BMP header!" << endl;
        return BMP_ERR;
    }

    elems = fwrite(&dib_header, sizeof(dib_header), 1, this->file);
    if (elems != 1) {
        cerr << "BMP: Could not write BITMAPv4 DIB header!" << endl;
        return BMP_ERR;
    }

    for (auto &c: this->colors) {
        elems = fwrite(&c, sizeof(struct color), 1, this->file);
        if (elems != 1) {
            cerr << "BMP: Could not write pixel!" << endl;
            return BMP_ERR;
        }
    }

    return BMP_OK;
}

int64_t
Bmp::get_filesize(void)
{
    return (int64_t)(this->file_size);
}
