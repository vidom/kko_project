/*
 * @brief  KKO 2016/2017 Conversion of image format GIF to BMP
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   05/2017
 * @file   bmp.h
 */

#ifndef __BMP_H__
#define __BMP_H__

#include <stdio.h>
#include <stdint.h>
#include <vector>

using namespace std;

#define BMP_OK 0
#define BMP_ERR -1

#define BMP_ID 0x4D42
#define BMP_LCS_WINDOWS_COLOR_SPACE 0x57696E20

class Bmp {

    /**
     * @brief BMP color.
     */
    struct color {
        uint8_t blue;
        uint8_t green;
        uint8_t red;
        uint8_t alpha;
    } __attribute__((packed));

    /**
     * @brief BMP header
     */
    struct bmp_header {
        uint16_t id;
        uint32_t file_size;
        uint16_t unused1;
        uint16_t unused2;
        uint32_t pixel_array_offset;
    } __attribute__((packed));

    /**
     * @brief BITMAPV4 DIB header
     */
    struct bitmapv4_header {
        uint32_t dib_header_size;
        uint32_t bitmap_width;
        uint32_t bitmap_height;
        uint16_t planes;
        uint16_t bits_per_pixel;
        uint32_t commpression_method;
        uint32_t image_size;
        uint32_t horizontal_res;
        uint32_t vertical_res;
        uint32_t colors;
        uint32_t important_colors;
        uint32_t red_bitmask;
        uint32_t green_bitmask;
        uint32_t blue_bitmask;
        uint32_t alpha_bitmask;
        uint32_t color_space;
        uint8_t  unused[48];
    } __attribute__((packed));

    FILE *file;
    uint16_t width;
    uint16_t height;
    vector<color> colors;
    uint32_t file_size;

public:
    /**
     * @brief Constructor.
     */
    Bmp(FILE *f, uint16_t w, uint16_t h);

    /**
     * @brief Sets pixel in image for BMP output.
     */
    void set_pixel(uint16_t row, uint16_t col, uint8_t red,
            uint8_t green, uint8_t blue, uint8_t transparent);

    /**
     * @brief Writes BMP image to file.
     * @return BMP_OK If success.
     *         BMP_ERR If failure.
     */
    int write(void);

    /**
     * @return Size of written BMP file.
     */
    int64_t get_filesize(void);
};

#endif
