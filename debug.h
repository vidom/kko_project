/*
 * @brief  KKO 2016/2017 Conversion of image format GIF to BMP
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   05/2017
 * @file   debug.h
 */

#ifndef __DEBUG_H_
#define __DEBUG_H_

#include <stdio.h>

#ifdef GIF2BMP_DEBUG
#define GIF2BMP_DEBUG_MSG(...) \
    do { \
        fprintf(stderr, "DEBUG|%s:% 4d:%s()| ", __FILE__, __LINE__, __func__); \
        fprintf(stderr, __VA_ARGS__); \
        fprintf(stderr, "\n"); \
    } while (0)
#define GIF2BMP_DEBUG_MSG_NN(...) \
    do { \
        fprintf(stderr, "DEBUG|%s:% 4d:%s()| ", __FILE__, __LINE__, __func__); \
        fprintf(stderr, __VA_ARGS__); \
    } while (0)
#define GIF2BMP_DEBUG_MSG_RAW(...) \
    do { \
        fprintf(stderr, __VA_ARGS__); \
    } while (0)
#else
#define GIF2BMP_DEBUG_MSG(...)
#define GIF2BMP_DEBUG_MSG_NN(...)
#define GIF2BMP_DEBUG_MSG_RAW(...)
#endif

#endif
