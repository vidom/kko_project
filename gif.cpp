/*
 * @brief  KKO 2016/2017 Conversion of image format GIF to BMP
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   05/2017
 * @file   gif.cpp
 */

#define __STDC_FORMAT_MACROS

/* System and standard library headers */
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include <iostream>
#include <vector>
#include <utility>

/* Own headers */
#include "gif.h"
#include "debug.h"

using namespace std;

Gif::logic_screen_desc::logic_screen_desc(void) :
    canvas_width{0},
    canvas_height{0},
    global_color_table_flag{0},
    color_resolution{0},
    sort_flag{0},
    global_color_table_size{0},
    background_color_index{0},
    pixel_aspect_ratio{0}
{
}

void
Gif::logic_screen_desc::dump(void)
{
    GIF2BMP_DEBUG_MSG("====================");
    GIF2BMP_DEBUG_MSG("Logical screen descriptor:");
    GIF2BMP_DEBUG_MSG("    canvas_width: %" PRIu16, canvas_width);
    GIF2BMP_DEBUG_MSG("    canvas_height: %" PRIu16, canvas_height);
    GIF2BMP_DEBUG_MSG("    global_color_table_flag: %" PRIu8,
            global_color_table_flag);
    GIF2BMP_DEBUG_MSG("    color_resolution: %" PRIu8, color_resolution);
    GIF2BMP_DEBUG_MSG("    sort_flag: %" PRIu8, sort_flag);
    GIF2BMP_DEBUG_MSG("    global_color_table_size: %" PRIu8,
            global_color_table_size);
    GIF2BMP_DEBUG_MSG("    background_color_index: %" PRIu8,
            background_color_index);
    GIF2BMP_DEBUG_MSG("    pixel_aspect_ratio: %" PRIu8, pixel_aspect_ratio);
    GIF2BMP_DEBUG_MSG("====================");
}

Gif::color_table::color_table(uint8_t m) :
    size{0},
    mask{m},
    colors{}
{
    this->size = Gif::color_table::mask_to_size(m);
}

void
Gif::color_table::dump(void)
{
    GIF2BMP_DEBUG_MSG("====================");
    GIF2BMP_DEBUG_MSG("Color table:");
    GIF2BMP_DEBUG_MSG("    size: %" PRIu16, size);
    GIF2BMP_DEBUG_MSG("    mask: %" PRIu8, mask);
#if 0
    GIF2BMP_DEBUG_MSG("    colors:");
    uint32_t i = 0;
    for (auto &c: colors) {
        GIF2BMP_DEBUG_MSG("        %" PRIu32
                ": (%" PRIu8 ",%" PRIu8 ",%" PRIu8 ")",
                i++, c.red, c.green, c.blue);
    }
#endif
    GIF2BMP_DEBUG_MSG("====================");
}

Gif::graphic_control_block::graphic_control_block(void) :
    size{0},
    disposal_method{0},
    user_input_flag{0},
    transparent_flag{0},
    delay_time{0},
    transparent_index{0}
{
}

void
Gif::graphic_control_block::dump(void)
{
    GIF2BMP_DEBUG_MSG("--------------------");
    GIF2BMP_DEBUG_MSG("Graphics Control Block:");
    GIF2BMP_DEBUG_MSG("    size: %" PRIu8, size);
    GIF2BMP_DEBUG_MSG("    disposal_method: %" PRIu8, disposal_method);
    GIF2BMP_DEBUG_MSG("    user_input_flag: %" PRIu8, user_input_flag);
    GIF2BMP_DEBUG_MSG("    transparent_flag: %" PRIu8, transparent_flag);
    GIF2BMP_DEBUG_MSG("    delay_time: %" PRIu16, delay_time);
    GIF2BMP_DEBUG_MSG("    transparent_index: %" PRIu16, transparent_index);
    GIF2BMP_DEBUG_MSG("--------------------");
}

Gif::image_block::image_block(void) :
    image_left{0},
    image_top{0},
    image_width{0},
    image_height{0},
    local_color_table_flag{0},
    interlace_flag{0},
    sort_flag{0},
    local_color_table_size{0},
    local_color_table{NULL},
    lzw_minimum_code_size{0},
    index_stream{}
{
}

Gif::image_block::~image_block(void)
{
    if (local_color_table != NULL) {
        delete local_color_table;
    }
}

void
Gif::image_block::dump(void)
{
    GIF2BMP_DEBUG_MSG("--------------------");
    GIF2BMP_DEBUG_MSG("Image block:");
    GIF2BMP_DEBUG_MSG("    image_left: %" PRIu16, image_left);
    GIF2BMP_DEBUG_MSG("    image_top: %" PRIu16, image_top);
    GIF2BMP_DEBUG_MSG("    image_width: %" PRIu16, image_width);
    GIF2BMP_DEBUG_MSG("    image_height: %" PRIu16, image_height);
    GIF2BMP_DEBUG_MSG("    local_color_table_flag: %" PRIu8,
            local_color_table_flag);
    GIF2BMP_DEBUG_MSG("    interlace_flag: %" PRIu8, interlace_flag);
    GIF2BMP_DEBUG_MSG("    sort_flag: %" PRIu8, sort_flag);
    GIF2BMP_DEBUG_MSG("    local_color_table_size: %" PRIu8,
            local_color_table_size);
    if (local_color_table != NULL) {
        local_color_table->dump();
    }
    GIF2BMP_DEBUG_MSG("    lzw_minimum_code_size: %" PRIu8,
            lzw_minimum_code_size);
    GIF2BMP_DEBUG_MSG("    index_stream size: %" PRIu64,
            (uint64_t)index_stream.size());
#if 0
    uint64_t i = 0;
    for (auto &index: index_stream) {
        if (i % 0x10 == 0) {
            GIF2BMP_DEBUG_MSG_NN("    ");
        } else {
            GIF2BMP_DEBUG_MSG_RAW(" ");
        }
        GIF2BMP_DEBUG_MSG_RAW("%03" PRIu16, index);
        if (i % 0x10 == 0xf) {
            GIF2BMP_DEBUG_MSG_RAW("\n");
        }
        i++;
    }
    if (i % 0x10 != 0) {
        GIF2BMP_DEBUG_MSG_RAW("\n");
    }
#endif
    GIF2BMP_DEBUG_MSG("--------------------");
}

Gif::block_group::block_group(void) :
    gcb{NULL},
    ib{NULL},
    type{Gif::block_group::block_type::UNKNOWN},
    block_data{}
{
}

Gif::block_group::~block_group(void)
{
    if (gcb != NULL) {
        delete gcb;
    }
    if (ib != NULL) {
        delete ib;
    }
}

const char *
Gif::block_group::block_type_to_text(Gif::block_group::block_type bt)
{
    switch (bt) {
        case Gif::block_group::block_type::IMAGE:
            return "'Image'";
        case Gif::block_group::block_type::PLAIN_TEXT:
            return "'Plain Text'";
        case Gif::block_group::block_type::APPLICATION:
            return "'Application'";
        case Gif::block_group::block_type::COMMENT:
            return "'Comment'";
        default:
            return "'unknown'";
    }
}

void
Gif::block_group::dump(void)
{
    GIF2BMP_DEBUG_MSG("====================");
    GIF2BMP_DEBUG_MSG("Group of blocks:");
    if (this->gcb != NULL) {
        this->gcb->dump();
    }
    if (this->ib != NULL) {
        this->ib->dump();
    }
    GIF2BMP_DEBUG_MSG("Type: %s", Gif::block_group::block_type_to_text(type));
    GIF2BMP_DEBUG_MSG("Data size: %" PRIu64, (uint64_t)block_data.size());
#if 0
    uint64_t i = 0;
    for (auto &u8: block_data) {
        if (i % 0x10 == 0) {
            GIF2BMP_DEBUG_MSG_NN("    ");
        } else {
            GIF2BMP_DEBUG_MSG_RAW(" ");
        }
        GIF2BMP_DEBUG_MSG_RAW("0x%02" PRIX8, u8);
        if (i % 0x10 == 0xf) {
            GIF2BMP_DEBUG_MSG_RAW("\n");
        }
        i++;
    }
    if (i % 0x10 != 0) {
        GIF2BMP_DEBUG_MSG_RAW("\n");
    }
#endif
    GIF2BMP_DEBUG_MSG("====================");
}

const uint16_t GIF_CODE_SIZE_LIMIT = 12;

Gif::lzw::lzw(struct block_group *_bg, struct color_table *_ct) :
    code_dict{},
    bg{_bg},
    color_table_ptr{_ct},
    cc_index{0},
    eoi_index{0},
    pos_byte{0},
    pos_bit{0},
    current_code_size{0}
{
    GIF2BMP_DEBUG_MSG("LZW init block_group pointer 0x%" PRIX64,
            (uint64_t)bg);
}

void
Gif::lzw::dump(void)
{
    GIF2BMP_DEBUG_MSG("--------------------");
    GIF2BMP_DEBUG_MSG("LZW:");
    GIF2BMP_DEBUG_MSG("    code_dict size: %" PRIu64,
            (uint64_t)code_dict.size());
    GIF2BMP_DEBUG_MSG("    cc_index: %" PRIu16, cc_index);
    GIF2BMP_DEBUG_MSG("    eoi_index: %" PRIu16, eoi_index);
    GIF2BMP_DEBUG_MSG("    pos_byte: %" PRIu16, pos_byte);
    GIF2BMP_DEBUG_MSG("    pos_bit: %" PRIu8, pos_bit);
    if (code_dict.size() > 0) {
        GIF2BMP_DEBUG_MSG("    greatest key: %" PRIu16,
                code_dict.rbegin()->first);
    }
    GIF2BMP_DEBUG_MSG("    current_code_size: %" PRIu8,
            current_code_size);
    GIF2BMP_DEBUG_MSG("--------------------");
}

int
Gif::lzw::check_init(void)
{
    if (this->bg == NULL) {
        cerr << "GIF: Missing block_group!" << endl;
        return GIF_ERR;
    }
    if (this->bg->ib == NULL) {
        cerr << "GIF: Missing image!" << endl;
        return GIF_ERR;
    }
    if (this->bg->block_data.size() == 0) {
        cerr << "GIF: No image data available!" << endl;
        return GIF_ERR;
    }
    if (this->color_table_ptr == NULL) {
        cerr << "GIF: No color table available!" << endl;
        return GIF_ERR;
    }
    if (this->bg->ib->lzw_minimum_code_size < 2 ||
            this->bg->ib->lzw_minimum_code_size > 8) {
        cerr << "GIF: LZW minimum code size (" <<
            int(this->bg->ib->lzw_minimum_code_size) <<
            ") is out of range <2-8>" << endl;
        return GIF_ERR;
    }
    this->bg->ib->index_stream.clear();
    this->dump();
    return GIF_OK;
}

int
Gif::lzw::init_dict(void)
{
    this->current_code_size = this->bg->ib->lzw_minimum_code_size + 1;
    this->cc_index = (1U << this->bg->ib->lzw_minimum_code_size);
    this->eoi_index = this->cc_index + 1;
    this->code_dict.clear();
    if (this->color_table_ptr->size != this->color_table_ptr->colors.size()) {
        cerr << "GIF: Unexpected color_table size mismatch!" << endl;
        return GIF_ERR;
    }
    uint16_t i = 0;
    uint16_t size = this->color_table_ptr->colors.size();
    for (i = 0; i < size; i++) {
        this->code_dict[i] = vector<uint16_t>{i};
    }
    this->code_dict[this->cc_index] = vector<uint16_t>{};
    this->code_dict[this->eoi_index] = vector<uint16_t>{};
    GIF2BMP_DEBUG_MSG("LZW code table (re)initialized!");
    this->dump();
    return GIF_OK;
}

int
Gif::lzw::next_code(uint16_t &code)
{
    if (this->current_code_size > GIF_CODE_SIZE_LIMIT) {
        cerr << "GIF: Unexpected current_code_size: " <<
            int(this->current_code_size) << endl;
        return GIF_ERR;
    }
    uint16_t new_code = 0;
    uint16_t set_bits = 0;
    while (set_bits < this->current_code_size) {
        if (this->pos_byte >= this->bg->block_data.size()) {
            cerr << "GIF: Unexpected end of data!" << endl;
            return GIF_ERR;
        }
        if (this->pos_bit >= 8) {
            cerr << "GIF: Ouf of byte range!" << endl;
            return GIF_ERR;
        }
        uint8_t byte = this->bg->block_data[this->pos_byte];
        uint16_t bit_val = (uint16_t)((byte >> this->pos_bit) & 0x1);
        new_code |= (bit_val << set_bits);
        set_bits++;
        (this->pos_bit)++;
        if (this->pos_bit >= 8) {
            this->pos_bit = 0;
            (this->pos_byte)++;
        }
    }

    code = new_code;
    return GIF_OK;
}

int
Gif::lzw::decode(void)
{
    uint16_t old_code;
    uint16_t new_code;
    bool repeat = true;

    if (this->check_init() != GIF_OK) {
        return GIF_ERR;
    }

    if (this->init_dict() != GIF_OK) {
        return GIF_ERR;
    }
    if (this->next_code(new_code) != GIF_OK ||
            new_code != this->cc_index) {
        cerr << "GIF: First code should be CC!" << endl;
        return GIF_ERR;
    }

    while (repeat == true) {
        repeat = false;

        if (this->next_code(new_code) != GIF_OK ||
                this->code_dict.find(new_code) == this->code_dict.end()) {
            cerr << "GIF: Could not get next code!" << endl;
            return GIF_ERR;
        }
        if (new_code == this->eoi_index) {
            GIF2BMP_DEBUG_MSG("Weird EOI right after CC!");
            break;
        }
        for (auto &i: this->code_dict[new_code]) {
            this->bg->ib->index_stream.push_back(i);
        }
        old_code = new_code;

        int ret;
        while ((ret = this->next_code(new_code)) == GIF_OK &&
                new_code != this->eoi_index) {
            if (new_code == this->cc_index) {
                if (this->init_dict() != GIF_OK) {
                    return GIF_ERR;
                }
                repeat = true;
                break;
            }

            uint16_t code_to_add;
            uint16_t k;
            if (this->code_dict.find(new_code) != this->code_dict.end()) {
                /* new_code is in code_dict */
                for (auto &i: this->code_dict[new_code]) {
                    this->bg->ib->index_stream.push_back(i);
                }
                /*
                 * It is assumed that there is at least one item
                 * at new_code index in code_dict.
                 */
                k = this->code_dict[new_code][0];
                code_to_add = this->code_dict.rbegin()->first + 1;
            } else {
                /* new_code is not in code_dict */
                /* old_code should be in code_dict */
                k = this->code_dict[old_code][0];
                for (auto &i: this->code_dict[old_code]) {
                    this->bg->ib->index_stream.push_back(i);
                }
                this->bg->ib->index_stream.push_back(k);
                code_to_add = new_code;
            }
            vector<uint16_t> indexes_to_add = this->code_dict[old_code];
            indexes_to_add.push_back(k);
            this->code_dict[code_to_add] = move(indexes_to_add);
            old_code = new_code;
            if (code_to_add == ((1 << this->current_code_size) - 1)) {
                /* Increase code size */
                if (this->current_code_size == GIF_CODE_SIZE_LIMIT) {
                    /*
                     * If code size reached GIF code size limit,
                     * reinit code table.
                     */
                    if (this->next_code(new_code) != GIF_OK ||
                            new_code != this->cc_index) {
                        cerr << "GIF: Next code should be CC!" << endl;
                        return GIF_ERR;
                    }
                    if (this->init_dict() != GIF_OK) {
                        return GIF_ERR;
                    }
                    repeat = true;
                    break;
                } else {
                    (this->current_code_size)++;
                    GIF2BMP_DEBUG_MSG("LZW current_code_size increased to: %"
                            PRIu16, this->current_code_size);
                    this->dump();
                }
            }
        }
        if (ret != GIF_OK) {
            cerr << "GIF: Could not reach EOI code!" << endl;
            return GIF_ERR;
        }
    }

    return GIF_OK;
}

const int GIF_HEADER_SIZE = 6;
const char *GIF_HEADER_SIGNATURE = "GIF";
const char *GIF_HEADER_VERSION_89a = "89a";
const char *GIF_HEADER_VERSION_87a = "87a";

const uint8_t TRAILER = 0x3BU;

const uint8_t EXTENSION_INTRODUCER = 0x21U;
const uint8_t IMAGE_SEPARATOR      = 0x2CU;

const uint8_t PLAIN_TEXT_LABEL      = 0x01U;
const uint8_t GRAPHIC_CONTROL_LABEL = 0xF9U;
const uint8_t APPLICATION_LABEL     = 0xFFU;
const uint8_t COMMENT_LABEL         = 0xFEU;

const uint8_t BLOCK_TERMINATOR = 0x0U;

static const char *
ext_label_to_text(uint8_t label)
{
    switch (label) {
        case PLAIN_TEXT_LABEL:
            return "'Plain Text Extension'";
        case GRAPHIC_CONTROL_LABEL:
            return "'Graphic Control Extension'";
        case APPLICATION_LABEL:
            return "'Application Extension'";
        case COMMENT_LABEL:
            return "'Comment Extension'";
        default:
            return "'<unknown>'";
    }
}

int
Gif::parse_header(void)
{
    uint8_t header[GIF_HEADER_SIZE];
    size_t elems;
    if (this->file == NULL) {
        cerr << "GIF: Missing file pointer!" << endl;
        return GIF_ERR;
    }
    elems = fread(header, sizeof(uint8_t), GIF_HEADER_SIZE, this->file);
    if (elems != GIF_HEADER_SIZE) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    GIF2BMP_DEBUG_MSG("Header: %c%c%c%c%c%c", header[0], header[1], header[2],
            header[3], header[4], header[5]);
    if (memcmp(header, GIF_HEADER_SIGNATURE,
                strlen(GIF_HEADER_SIGNATURE)) != 0) {
        cerr << "GIF: Wrong header signature!" << endl;
        return GIF_ERR;
    }
    if (memcmp(&header[3], GIF_HEADER_VERSION_89a,
                strlen(GIF_HEADER_VERSION_89a)) != 0 &&
            memcmp(&header[3], GIF_HEADER_VERSION_87a,
                strlen(GIF_HEADER_VERSION_87a)) != 0) {
        cerr << "GIF: Unsupported header version!" << endl;
        return GIF_ERR;
    }
    return GIF_OK;
}

int
Gif::parse_logic_screen_desc(void)
{
    uint16_t u16;
    uint8_t u8;
    size_t elems;
    if (this->file == NULL) {
        cerr << "GIF: Missing file pointer!" << endl;
        return GIF_ERR;
    }

    elems = fread(&u16, sizeof(u16), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    this->lsd.canvas_width = u16;

    elems = fread(&u16, sizeof(u16), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    this->lsd.canvas_height = u16;

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    this->lsd.global_color_table_flag = (u8 >> 7) & 0x1;
    this->lsd.color_resolution = (u8 >> 4) & 0x7;
    this->lsd.sort_flag = (u8 >> 3) & 0x1;
    this->lsd.global_color_table_size = (u8) & 0x7;

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    this->lsd.background_color_index = u8;

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    this->lsd.pixel_aspect_ratio = u8;

    /* Print logical screen descriptor info for debug purposes. */
    this->lsd.dump();

    return GIF_OK;
}

int
Gif::parse_color_table(Gif::color_table &ct)
{
    uint32_t i;
    uint32_t size = ct.size;
    vector<Gif::color> vec_colors;

    if (this->file == NULL) {
        cerr << "GIF: Missing file pointer!" << endl;
        return GIF_ERR;
    }
    for (i = 0; i < size; i++) {
        size_t elems;
        uint8_t buf[3];
        elems = fread(buf, sizeof(uint8_t), 3, this->file);
        if (elems != 3) {
            cerr << "GIF: fread() failure!" << endl;
            return GIF_ERR;
        }
        vec_colors.push_back(Gif::color{buf[0], buf[1], buf[2]});
    }

    ct.colors = move(vec_colors);
    /* Print color table for debug purposes. */
    ct.dump();
    return GIF_OK;
}

int
Gif::parse_graphic_control_ext(block_group &bg)
{
    uint16_t u16;
    uint8_t u8;
    size_t elems;

    if (this->file == NULL) {
        cerr << "GIF: Missing file pointer!" << endl;
        return GIF_ERR;
    }

    if (bg.gcb != NULL) {
        delete bg.gcb;
    }
    bg.gcb = new Gif::graphic_control_block{};

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    if (u8 != 0x4U) {
        cerr << "GIC: Expected Graphic Control Block size is " <<
            0x4U << " but got " << u8 << endl;
        return GIF_ERR;
    }
    bg.gcb->size = u8;

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    bg.gcb->disposal_method = (u8 >> 2) & 0x7;
    bg.gcb->user_input_flag = (u8 >> 1) & 0x1;
    bg.gcb->transparent_flag = (u8) & 0x1;

    elems = fread(&u16, sizeof(u16), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    bg.gcb->delay_time = u16;

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    bg.gcb->transparent_index = u8;

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    if (u8 != BLOCK_TERMINATOR) {
        cerr << "GIF: Expected Block Terminator " <<
            BLOCK_TERMINATOR << " but got " << u8 << endl;
        return GIF_ERR;
    }

    return GIF_OK;
}

int
Gif::parse_block_size(void)
{
    size_t elems;
    uint8_t u8;

    if (this->file == NULL) {
        cerr << "GIF: Missing file pointer!" << endl;
        return GIF_ERR;
    }
    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    if (u8 > 0) {
        uint8_t *buf = new uint8_t[u8];
        elems = fread(buf, sizeof(uint8_t), u8, this->file);
        delete[] buf;
        if (elems != u8) {
            cerr << "GIF: fread() failure!" << endl;
            return GIF_ERR;
        }
    }

    return GIF_OK;
}

int
Gif::parse_subblocks(block_group &bg)
{
    size_t elems;
    uint8_t u8;
    vector<uint8_t> data_vec{};

    if (this->file == NULL) {
        cerr << "GIF: Missing file pointer!" << endl;
        return GIF_ERR;
    }

    while ((elems = fread(&u8, sizeof(u8), 1, this->file)) == 1 &&
            u8 != 0) {
        uint8_t *buf = new uint8_t[u8];
        uint16_t i;
        uint16_t subblock_size = (uint16_t)u8;
        elems = fread(buf, sizeof(uint8_t), u8, this->file);
        if (elems != u8) {
            cerr << "GIF: fread() failure!" << endl;
            delete[] buf;
            return GIF_ERR;
        }
        for (i = 0; i < subblock_size; i++) {
            data_vec.push_back(buf[i]);
        }
        delete[] buf;
    }
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }

    bg.block_data = move(data_vec);
    return GIF_OK;
}

int
Gif::parse_plain_text_ext(block_group &bg)
{
    if (this->parse_block_size() != GIF_OK) {
        cerr << "Could not parse block size!" << endl;
        return GIF_ERR;
    }
    if (this->parse_subblocks(bg) != GIF_OK) {
        cerr << "Could not parse data sub-blocks!" << endl;
        return GIF_ERR;
    }
    bg.type = Gif::block_group::block_type::PLAIN_TEXT;
    return GIF_OK;
}

int
Gif::parse_application_ext(block_group &bg)
{
    if (this->parse_block_size() != GIF_OK) {
        cerr << "Could not parse block size!" << endl;
        return GIF_ERR;
    }
    if (this->parse_subblocks(bg) != GIF_OK) {
        cerr << "Could not parse data sub-blocks!" << endl;
        return GIF_ERR;
    }
    bg.type = Gif::block_group::block_type::APPLICATION;
    return GIF_OK;
}

int
Gif::parse_comment_ext(block_group &bg)
{
    if (this->parse_block_size() != GIF_OK) {
        cerr << "Could not parse block size!" << endl;
        return GIF_ERR;
    }
    if (this->parse_subblocks(bg) != GIF_OK) {
        cerr << "Could not parse data sub-blocks!" << endl;
        return GIF_ERR;
    }
    bg.type = Gif::block_group::block_type::COMMENT;
    return GIF_OK;
}

int
Gif::parse_extension(Gif::block_group &bg)
{
    size_t elems;
    uint8_t u8;

    if (this->file == NULL) {
        cerr << "GIF: Missing file pointer!" << endl;
        return GIF_ERR;
    }

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    GIF2BMP_DEBUG_MSG("Extension type to parse: %s", ext_label_to_text(u8));
    if (u8 == GRAPHIC_CONTROL_LABEL) {
        /* Expected Graphic Control Extension */
        if (this->parse_graphic_control_ext(bg) != GIF_OK) {
            cerr << "GIF: Could not parse Graphic Control Extension!" << endl;
            return GIF_ERR;
        }

        elems = fread(&u8, sizeof(u8), 1, this->file);
        if (elems != 1) {
            cerr << "GIF: fread() failure!" << endl;
            return GIF_ERR;
        }
        if (u8 == EXTENSION_INTRODUCER) {
            /* Expected Plain Text Extension */
            elems = fread(&u8, sizeof(u8), 1, this->file);
            if (elems != 1) {
                cerr << "GIF: fread() failure!" << endl;
                return GIF_ERR;
            }
            GIF2BMP_DEBUG_MSG("Extension type to parse: %s",
                    ext_label_to_text(u8));
            if (u8 != PLAIN_TEXT_LABEL ||
                    this->parse_plain_text_ext(bg) != GIF_OK) {
                cerr << "GIF: Could not parse Plain Text Extension!" << endl;
                return GIF_ERR;
            }
        } else if (u8 == IMAGE_SEPARATOR) {
            if (this->parse_image(bg) != GIF_OK) {
                cerr << "GIF: Could not parse image!" << endl;
                return GIF_ERR;
            }
        }
    } else if (u8 == PLAIN_TEXT_LABEL) {
        /* Expected Plain Text Extension */
        if (this->parse_plain_text_ext(bg) != GIF_OK) {
            cerr << "GIF: Could not parse Plain Text Extension!" << endl;
            return GIF_ERR;
        }
    } else if (u8 == APPLICATION_LABEL) {
        /* Expected Application Extension */
        if (this->parse_application_ext(bg) != GIF_OK) {
            cerr << "GIF: Could not parse Application Extension!" << endl;
            return GIF_ERR;
        }
    } else if (u8 == COMMENT_LABEL) {
        /* Expected Comment Extension */
        if (this->parse_comment_ext(bg) != GIF_OK) {
            cerr << "GIF: Could not parse Comment Extension!" << endl;
            return GIF_ERR;
        }
    } else {
        cerr << "GIF: Unexpected byte value!" << endl;
        return GIF_ERR;
    }

    return GIF_OK;
}

void
Gif::deinterlace(Gif::image_block *ib)
{
    if (ib != NULL && ib->interlace_flag == 1) {
        vector<uint16_t> new_vec = ib->index_stream;
        uint64_t row = 0;
        uint64_t w = ib->image_width;
        uint64_t h = ib->image_height;
        uint64_t i;
        uint64_t j;

        for (i = 0; i < h; i += 8) {
            for (j = 0; j < w; j++) {
                new_vec[(i * w) + j] = ib->index_stream[(row * w) + j];
            }
            row++;
        }
        for (i = 4; i < h; i += 8) {
            for (j = 0; j < w; j++) {
                new_vec[(i * w) + j] = ib->index_stream[(row * w) + j];
            }
            row++;
        }
        for (i = 2; i < h; i += 4) {
            for (j = 0; j < w; j++) {
                new_vec[(i * w) + j] = ib->index_stream[(row * w) + j];
            }
            row++;
        }
        for (i = 1; i < h; i += 2) {
            for (j = 0; j < w; j++) {
                new_vec[(i * w) + j] = ib->index_stream[(row * w) + j];
            }
            row++;
        }

        ib->index_stream = move(new_vec);
    }
}

int
Gif::parse_image(Gif::block_group &bg)
{
    uint16_t u16;
    uint8_t u8;
    size_t elems;

    if (this->file == NULL) {
        cerr << "GIF: Missing file pointer!" << endl;
        return GIF_ERR;
    }

    if (bg.ib != NULL) {
        delete bg.ib;
    }
    bg.ib = new Gif::image_block{};

    elems = fread(&u16, sizeof(u16), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    bg.ib->image_left = u16;

    elems = fread(&u16, sizeof(u16), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    bg.ib->image_top = u16;

    elems = fread(&u16, sizeof(u16), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    bg.ib->image_width = u16;

    elems = fread(&u16, sizeof(u16), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    bg.ib->image_height = u16;

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    bg.ib->local_color_table_flag = (u8 >> 7) & 0x1;
    bg.ib->interlace_flag = (u8 >> 6) & 0x1;
    bg.ib->sort_flag = (u8 >> 5) & 0x1;
    bg.ib->local_color_table_size = (u8) & 0x7;

    if (bg.ib->local_color_table_flag == 1) {
        /* Local table is present, parse it. */
        bg.ib->local_color_table = new Gif::color_table{
            bg.ib->local_color_table_size};
        if (this->parse_color_table(*(bg.ib->local_color_table)) != GIF_OK) {
            cerr << "GIF: Could not parse local color table!" << endl;
            return GIF_ERR;
        }
    }

    elems = fread(&u8, sizeof(u8), 1, this->file);
    if (elems != 1) {
        cerr << "GIF: fread() failure!" << endl;
        return GIF_ERR;
    }
    bg.ib->lzw_minimum_code_size = u8;

    if (this->parse_subblocks(bg) != GIF_OK) {
        cerr << "GIF: Could not parse data sub-blocks!" << endl;
        return GIF_ERR;
    }

    lzw lzw{&bg, ((bg.ib->local_color_table != NULL) ?
            bg.ib->local_color_table : this->global_color_table)};
    if (lzw.decode() != GIF_OK) {
        cerr << "GIF: Could not decompress LZW!" << endl;
        return GIF_ERR;
    }

    if (((uint64_t)(bg.ib->image_width)) * ((uint64_t)(bg.ib->image_height)) !=
            ((uint64_t)bg.ib->index_stream.size())) {
        cerr << "GIF: Image index stream does not match dimensions!" << endl;
        return GIF_ERR;
    }

    if (bg.ib->interlace_flag == 1) {
        this->deinterlace(bg.ib);
    }

    bg.type = Gif::block_group::block_type::IMAGE;
    return GIF_OK;
}

int
Gif::parse_block_group(uint8_t u8)
{
    Gif::block_group *bg = new Gif::block_group{};
    if (u8 == EXTENSION_INTRODUCER) {
        /* Expected extension */
        if (this->parse_extension(*bg) != GIF_OK) {
            cerr << "GIF: Could not parse extension!" << endl;
            delete bg;
            return GIF_ERR;
        }
    } else if (u8 == IMAGE_SEPARATOR) {
        /* Expected image */
        if (this->parse_image(*bg) != GIF_OK) {
            cerr << "GIF: Could not parse image!" << endl;
            delete bg;
            return GIF_ERR;
        }
    } else {
        cerr << "GIF: Unexpected byte value!" << endl;
        delete bg;
        return GIF_ERR;
    }
    /* Print parsed block group */
    bg->dump();
    this->block_groups.push_back(bg);
    return GIF_OK;
}

Gif::Gif(FILE *f) :
    file{f},
    lsd{},
    global_color_table{NULL},
    block_groups{}
{
}

Gif::~Gif(void)
{
    if (global_color_table != NULL) {
        delete global_color_table;
    }
    for (auto &bg: block_groups) {
        delete bg;
    }
}

int
Gif::parse(void)
{
    size_t elems = 0;
    uint8_t u8;
    if (this->parse_header() != GIF_OK) {
        cerr << "GIF: Could not parse header!" << endl;
        return GIF_ERR;
    }
    if (this->parse_logic_screen_desc() != GIF_OK) {
        cerr << "GIF: Could not parse logical screen descriptor!" << endl;
        return GIF_ERR;
    }
    if (this->lsd.global_color_table_flag == 1) {
        /* Global color table is present, parse it. */
        this->global_color_table = new Gif::color_table{
                this->lsd.global_color_table_size};
        if (this->parse_color_table(*(this->global_color_table)) != GIF_OK) {
            cerr << "GIF: Could not parse global color table!" << endl;
            return GIF_ERR;
        }
    }

    this->block_groups.clear();
    while ((elems = fread(&u8, sizeof(u8), 1, this->file)) == 1 &&
            u8 != TRAILER) {
        if (this->parse_block_group(u8) != GIF_OK) {
            cerr << "GIF: Could not parse group of blocks!" << endl;
            return GIF_ERR;
        }
    }
    if (elems != 1) {
        cerr << "GIF: Trailer not found!" << endl;
        return GIF_ERR;
    }

    return GIF_OK;
}

int
Gif::get_dimensions(uint16_t &width, uint16_t &height)
{
    for (auto &bg: this->block_groups) {
        if (bg != NULL && bg->ib != NULL) {
            width = bg->ib->image_width;
            height = bg->ib->image_height;
            return GIF_OK;
        }
    }
    return GIF_ERR;
}

int
Gif::get_pixel(uint16_t row, uint16_t col, uint8_t &red,
            uint8_t &green, uint8_t &blue, uint8_t &transparent)
{
    for (auto &bg: this->block_groups) {
        if (bg != NULL && bg->ib != NULL) {
            uint16_t index = bg->ib->index_stream[
                ((uint64_t)(row)) * ((uint64_t)(bg->ib->image_width)) +
                ((uint64_t)(col))];
            struct color_table *ct =
                (bg->ib->local_color_table != NULL) ? bg->ib->local_color_table :
                this->global_color_table;
            if (ct == NULL) {
                cerr << "GIF: Missing color table!" << endl;
                return GIF_ERR;
            }
            struct color c = ct->colors[index];
            red = c.red;
            green = c.green;
            blue = c.blue;
            if (bg->gcb != NULL && bg->gcb->transparent_flag == 1
                    && index == bg->gcb->transparent_index) {
                transparent = 0;
            } else {
                transparent = 255;
            }
            return GIF_OK;
        }
    }
    return GIF_ERR;
}
