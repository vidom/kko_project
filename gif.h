/*
 * @brief  KKO 2016/2017 Conversion of image format GIF to BMP
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   05/2017
 * @file   gif.h
 */

#ifndef __GIF_H__
#define __GIF_H__

#include <stdio.h>
#include <stdint.h>

#include <vector>
#include <map>
using namespace std;

#define GIF_OK 0
#define GIF_ERR -1

class Gif {

    /**
     * @brief Representation of Logical Screen Descriptor.
     */
    struct logic_screen_desc {
        uint16_t canvas_width;
        uint16_t canvas_height;
        uint8_t global_color_table_flag : 1;
        uint8_t color_resolution : 3;
        uint8_t sort_flag : 1;
        uint8_t global_color_table_size : 3;
        uint8_t background_color_index;
        uint8_t pixel_aspect_ratio;

        /**
         * @brief Constructor.
         */
        logic_screen_desc(void);

        /**
         * @brief Prints state of the object for debug purposes.
         */
        void dump(void);
    };

    /**
     * @brief Representation of gif color.
     */
    struct color {
        uint8_t red;
        uint8_t green;
        uint8_t blue;
    };

    /**
     * @brief Representation of Color Table (global or local).
     */
    struct color_table {
        uint16_t size;
        uint8_t mask;
        vector<color> colors;

        /**
         * @brief Constructor.
         */
        color_table(uint8_t m);

        /**
         * @brief Prints state of the object for debug purposes.
         */
        void dump(void);

    private:

        /**
         * @brief Counts color_table size from given mask.
         *
         * Maximum mask value can be 0x7.
         * If mask is greater, 0x7 is used.
         */
        static uint16_t mask_to_size(uint8_t mask)
        {
            mask &= 0x7;
            return ((uint16_t)0x2U) << mask;
        }
    };

    /**
     * @brief Representation of Graphics Control Extension block.
     */
    struct graphic_control_block {
        uint8_t size;
        uint8_t disposal_method : 3;
        uint8_t user_input_flag : 1;
        uint8_t transparent_flag : 1;
        uint16_t delay_time;
        uint8_t transparent_index;

        /**
         * @brief Constructor.
         */
        graphic_control_block(void);

        /**
         * @brief Prints state of the object for debug purposes.
         */
        void dump(void);
    };

    /**
     * @brief Holds information about image.
     *
     * Includes:
     * - Image Descriptor
     * - Local Color Table
     * - LZW minimum code size from Image Data
     * - index stream gained from LZW decompression
     */
    struct image_block {
        uint16_t image_left;
        uint16_t image_top;
        uint16_t image_width;
        uint16_t image_height;
        uint8_t local_color_table_flag : 1;
        uint8_t interlace_flag : 1;
        uint8_t sort_flag : 1;
        uint8_t local_color_table_size : 3;
        color_table *local_color_table;
        uint8_t lzw_minimum_code_size;
        vector<uint16_t> index_stream;

        /**
         * @brief Constructor.
         */
        image_block(void);

        /**
         * @brief Destructor.
         */
        ~image_block(void);

        /**
         * @brief Prints state of the object for debug purposes.
         */
        void dump(void);
    };

    /**
     * @brief Holds information from group of blocks which can repeat
     *        multiple times.
     *
     * Includes:
     * - pointer to structure with Graphics Control Extension block
     *   (if exists)
     * - pointer to structure with image information (if exists)
     * - type of blocks
     * - joined data from data sub-blocks
     */
    struct block_group {
        enum block_type {
            UNKNOWN = 0,
            IMAGE = 1,
            PLAIN_TEXT = 2,
            APPLICATION = 3,
            COMMENT = 4,
        };

        graphic_control_block *gcb;
        image_block *ib;
        enum block_type type;
        vector<uint8_t> block_data;

        /**
         * @brief Constructor.
         */
        block_group(void);

        /**
         * @brief Destructor.
         */
        ~block_group(void);

        /**
         * @brief Prints state of the object for debug purposes.
         */
        void dump(void);

        /**
         * @brief Converts numeral value of block type to text.
         */
        static const char *block_type_to_text(block_type bt);
    };

    /**
     * @brief Class handling LZW decompression.
     */
    class lzw {
        map<uint16_t, vector<uint16_t>> code_dict;
        block_group *bg;
        color_table *color_table_ptr;
        uint16_t cc_index;
        uint16_t eoi_index;
        uint16_t pos_byte;
        uint8_t pos_bit;
        uint8_t current_code_size;

        /**
         * @brief Checks if structure is correctly initialized.
         * @return GIF_OK If success.
         *         GIF_ERR If failure.
         */
        int check_init(void);

        /**
         * @brief (Re)initializes code table (dictionary).
         * @return GIF_OK If success.
         *         GIF_ERR If failure.
         */
        int init_dict(void);

        /**
         * @brief Gets next code from image data.
         * @return GIF_OK If success.
         *         GIF_ERR If failure.
         */
        int next_code(uint16_t &code);

    public:

        /**
         * @brief Constructor.
         */
        lzw(struct block_group *_bg, struct color_table *_ct);

        /**
         * @brief Executes LZW decompression with given image.
         * @return GIF_OK If success.
         *         GIF_ERR If failure.
         */
        int decode(void);

        /**
         * @brief Prints state of the object for debug purposes.
         */
        void dump(void);
    };

    FILE *file;
    logic_screen_desc lsd;
    color_table *global_color_table;
    vector<block_group *> block_groups;

    /**
     * @brief Parses GIF header.
     * @return GIF_OK If header is ok.
     *         GIF_ERR If header could not be parsed.
     */
    int parse_header(void);

    /**
     * @brief Parses GIF logical screen descriptor.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_logic_screen_desc(void);

    /**
     * @brief Parses GIF color table.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_color_table(color_table &ct);

    /**
     * @brief Parses GIF Graphic Control Extension.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_graphic_control_ext(block_group &bg);

    /**
     * @brief Parses GIF extension block size.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_block_size(void);

    /**
     * @brief Parses GIF Sub-blocks data.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_subblocks(block_group &bg);

    /**
     * @brief Parses GIF Plain Text Extension.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_plain_text_ext(block_group &bg);

    /**
     * @brief Parses GIF Application Extension.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_application_ext(block_group &bg);

    /**
     * @brief Parses GIF Comment Extension.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_comment_ext(block_group &bg);

    /**
     * @brief Parses GIF general extension, calls specific extension parsers.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_extension(block_group &bg);

    /**
     * @brief Deinterlaces interlaced image.
     */
    void deinterlace(image_block *ib);

    /**
     * @brief Parses GIF image.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_image(block_group &bg);

    /**
     * @brief Parses group of blocks.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse_block_group(uint8_t u8);

public:

    /**
     * @brief Constructor
     */
    Gif(FILE *f);

    /**
     * @brief Destructor
     */
    ~Gif(void);

    /**
     * @brief Parses file in gif format.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int parse(void);

    /**
     * @brief Gets width and height of the image in parsed file.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     */
    int get_dimensions(uint16_t &width, uint16_t &height);

    /**
     * @brief Gets pixel from parsed GIF image.
     * @return GIF_OK If success.
     *         GIF_ERR If failure.
     *
     * Rows are indexed from 0 and start at the top.
     * Columns are indexed from 0 and start from the left.
     */
    int get_pixel(uint16_t row, uint16_t col, uint8_t &red,
            uint8_t &green, uint8_t &blue, uint8_t &transparent);
};

#endif
