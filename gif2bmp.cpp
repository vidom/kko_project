/*
 * @brief  KKO 2016/2017 Conversion of image format GIF to BMP
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   05/2017
 * @file   gif2bmp.cpp
 */

/* System and standard library headers */
#include <stdio.h>
#include <errno.h>
#include <string.h>

/* Own headers */
#include "gif2bmp.h"
#include "gif.h"
#include "bmp.h"

int
gif2bmp(tGIF2BMP *record, FILE *ifs, FILE *ofs)
{
    uint32_t i;
    uint32_t j;
    uint16_t width;
    uint16_t height;
    int ret;
    int64_t size;

    Gif gif{ifs};
    if (gif.parse() != GIF_OK) {
        return GIF2BMP_ERR;
    }
    if (gif.get_dimensions(width, height) != GIF_OK) {
        fprintf(stderr, "Could not get GIF dimensions!");
        return GIF2BMP_ERR;
    }

    Bmp bmp{ofs, width, height};
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            uint8_t red;
            uint8_t green;
            uint8_t blue;
            uint8_t transparent;
            if (gif.get_pixel(i, j, red, green, blue, transparent) != GIF_OK) {
                fprintf(stderr, "Could not get pixel value!");
                return GIF2BMP_ERR;
            }
            bmp.set_pixel(height - i - 1, j, red, green, blue, transparent);
        }
    }
    if (bmp.write() != BMP_OK) {
        fprintf(stderr, "Could not write BMP file!");
        return GIF2BMP_ERR;
    }

    ret = fseek(ifs, 0L, SEEK_END);
    if (ret != 0) {
        fprintf(stderr, "Input file fseek() error: %s\n", strerror(errno));
        return GIF2BMP_ERR;
    }
    size = ftell(ifs);
    if (size < 0) {
        fprintf(stderr, "Input file ftell() error: %s\n", strerror(errno));
        return GIF2BMP_ERR;
    }
    record->gifSize = size;
    record->bmpSize = bmp.get_filesize();
    return GIF2BMP_OK;
}
