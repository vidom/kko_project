/*
 * @brief  KKO 2016/2017 Conversion of image format GIF to BMP
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   05/2017
 * @brief  gif2bmp.h
 */

#ifndef __GIF2BMP_H__
#define __GIF2BMP_H__

#include <stdio.h>
#include <stdint.h>

#define GIF2BMP_OK 0
#define GIF2BMP_ERR -1

typedef struct {
    int64_t bmpSize;
    int64_t gifSize;
} tGIF2BMP;

/*
 * @brief Converts input file in GIF format to BMP and writes to output file
 * @param gif2bmp    Pointer to conversion report
 * @param inputFile  Open input file
 * @param outputFile Open output file
 * @return GIF2BMP_OK  If OK
 *         GIF2BMP_ERR If error
 */
int gif2bmp(tGIF2BMP *gif2bmp, FILE *inputFile, FILE *outputFile);

#endif
