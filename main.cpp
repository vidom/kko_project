/*
 * @brief  KKO 2016/2017 Conversion of image format GIF to BMP
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   05/2017
 * @file   main.cpp
 */

#define __STDC_FORMAT_MACROS

/* System and standard library headers. */
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>

/* Own headers. */
#include "gif2bmp.h"

static void
print_help(FILE *s, const char *name)
{
    fprintf(s, "Usage:\n");
    fprintf(s, "    %s [-i <ifile>] [-o <ofile>] [-l <logfile>] [-h]\n", name);
    fprintf(s, "        -i <ifile>   ... <ifile> is name of input file\n");
    fprintf(s, "        -o <ofile>   ... <ofile> is name of output file\n");
    fprintf(s, "        -l <logfile> ... <logfile> is name of output file\n");
    fprintf(s, "                         for output record\n");
}

int
main(int argc, char *argv[])
{
    int opt = 0;
    const char *ifile = NULL;
    const char *ofile = NULL;
    const char *logfile = NULL;
    FILE *ifs = stdin;
    FILE *ofs = stdout;
    FILE *lfs = NULL;
    int ret;
    tGIF2BMP record;

    /* Process command line options. */
    while ((opt = getopt(argc, argv, "i:o:l:h")) != -1) {
        switch (opt) {
            case 'i':
                ifile = optarg;
                break;
            case 'o':
                ofile = optarg;
                break;
            case 'l':
                logfile = optarg;
                break;
            case 'h':
                print_help(stdout, argv[0]);
                return EXIT_SUCCESS;
            default:
                /* Anything else is wrong, report error and return. */
                fprintf(stderr, "Wrong options!\n");
                print_help(stderr, argv[0]);
                return EXIT_FAILURE;
        }
    }

    if (argc > optind) {
        /* Name arguments are not allowed. */
        fprintf(stderr, "Unexpected name argument '%s'\n", argv[optind]);
        print_help(stderr, argv[0]);
        return EXIT_FAILURE;
    }

    /* Open input file */
    if (ifile != NULL) {
        ifs = fopen(ifile, "rb");
        if (ifs == NULL) {
            fprintf(stderr, "Input file '%s' fopen() error: %s\n", ifile,
                    strerror(errno));
            return EXIT_FAILURE;
        }
    }
    /* Open output file */
    if (ofile != NULL) {
        ofs = fopen(ofile, "wb");
        if (ofs == NULL) {
            fprintf(stderr, "Output file '%s' fopen() error: %s\n", ofile,
                    strerror(errno));
            fclose(ifs);
            return EXIT_FAILURE;
        }
    }
    /* Open log file */
    if (logfile != NULL) {
        lfs = fopen(logfile, "w");
        if (lfs == NULL) {
            fprintf(stderr, "Log file '%s' fopen() error: %s\n", logfile,
                    strerror(errno));
            fclose(ifs);
            fclose(ofs);
            return EXIT_FAILURE;
        }
    }

    /* Make conversion */
    ret = gif2bmp(&record, ifs, ofs);
    if (ret != GIF2BMP_OK) {
        fprintf(stderr, "Error occurred during conversion!\n");
        fclose(ifs);
        fclose(ofs);
        if (logfile != NULL) {
            fclose(lfs);
        }
        return EXIT_FAILURE;
    }

    /* Print conversion record */
    if (logfile != NULL) {
        fprintf(lfs, "login = xvidom00\n");
        fprintf(lfs, "uncodedSize = %" PRId64 "\n", record.gifSize);
        fprintf(lfs, "codedSize = %" PRId64 "\n", record.bmpSize);
        fclose(lfs);
    }

    /* Close files and finish */
    fclose(ifs);
    fclose(ofs);
    return EXIT_SUCCESS;
}
